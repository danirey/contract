# Copyright 2022 Jamotion - Renzo Meister
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Fixes for Contracts Management",
    "version": "2.0.1.0.0",
    "category": "Contract Management",
    "license": "AGPL-3",
    "author": "Jamotion GmbH, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/contract",
    "depends": ["contract"],
    "development_status": "Production/Stable",
    "data": [
        'views/contract_template_views.xml',
    ],
    "installable": True,
}
