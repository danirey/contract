# Flectra Community / contract

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[contract_sale_generation](contract_sale_generation/) | 2.0.1.0.1| Contracts Management - Recurring Sales
[agreement](agreement/) | 2.0.1.1.0| Adds an agreement object
[agreement_account](agreement_account/) | 2.0.1.0.0| Agreement on invoices
[contract_invoice_start_end_dates](contract_invoice_start_end_dates/) | 2.0.1.0.0| Contract Invoice Start End Dates
[contract_update_last_date_invoiced](contract_update_last_date_invoiced/) | 2.0.1.0.0|         This module allows to update the last date invoiced if invoices are deleted.
[contract_sale](contract_sale/) | 2.0.1.2.0| Contract from Sale
[agreement_legal](agreement_legal/) | 2.0.2.4.1| Manage Agreements, LOI and Contracts
[agreement_tier_validation](agreement_tier_validation/) | 2.0.1.0.1| Extends the functionality of Agreement to support a tier validation process.
[agreement_sale](agreement_sale/) | 2.0.1.0.0| Agreement on sales
[contract_payment_mode](contract_payment_mode/) | 2.0.1.0.0| Payment mode in contracts and their invoices
[agreement_serviceprofile](agreement_serviceprofile/) | 2.0.1.1.0| Adds an Agreement Service Profile object
[agreement_repair](agreement_repair/) | 2.0.1.0.0| Link repair orders to an agreement
[contract](contract/) | 2.0.2.9.1| Recurring - Contracts Management
[agreement_maintenance](agreement_maintenance/) | 2.0.1.2.0| Manage maintenance agreements and contracts
[contract_variable_quantity](contract_variable_quantity/) | 2.0.1.0.0| Variable quantity in contract recurrent invoicing
[contract_sale_tag](contract_sale_tag/) | 2.0.1.0.1|         Allows to transmit contract tags to sale order (through sale_generation)
[agreement_project](agreement_project/) | 2.0.1.0.0| Link projects to an agreement
[contract_mandate](contract_mandate/) | 2.0.1.0.0| Mandate in contracts and their invoices
[agreement_legal_sale](agreement_legal_sale/) | 2.0.1.0.1| Create an agreement when the sale order is confirmed
[agreement_stock](agreement_stock/) | 2.0.1.0.1| Link picking to an agreement
[agreement_mrp](agreement_mrp/) | 2.0.1.0.0| Link manufacturing orders to an agreement
[contract_delivery_zone](contract_delivery_zone/) | 2.0.1.0.0|         Allows to remind the delivery zone defined on the partner on contract level.


