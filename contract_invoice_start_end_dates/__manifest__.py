# Copyright 2019-2020 Akretion France (http://www.akretion.com/)
# License AGPL-3 - See http://www.gnu.org/licenses/agpl-3.0.html

{
    "name": "Contract Invoice Start End Dates",
    "version": "2.0.1.0.0",
    "category": "Contract Management",
    "author": "Akretion, Odoo Community Association (OCA)",
    "maintainers": ["florian-dacosta"],
    "website": "https://gitlab.com/flectra-community/contract",
    "depends": [
        "account_invoice_start_end_dates",
        "contract",
    ],
    "data": [],
    "license": "AGPL-3",
    "installable": True,
}
